source TCL/testbench.tcl

# Compile files
compile_project

# Launch simulation
simulate start

#Enable JTAG
send_command 0x00 0x01 0x00 0x06 0xFF000000

#TDO [31:0]
send_command 0x00 0x02 0x13 0x00 0x12345678

#TMS [31:0]
send_command 0x00 0x03 0x13 0x40 0x000000FF

#Configure 
send_command 0x00 0x04 0x13 0x80 0x00004220

#Go
send_command 0x00 0x05 0x13 0xa2 0x00000000

#Read TDI
send_command 0x00 0x06 0x13 0x01 0x00000000